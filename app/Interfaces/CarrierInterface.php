<?php

namespace App\Interfaces;

use App\Call;
use App\Contact;
use App\Message;


interface CarrierInterface
{
	public function selectContact(Contact $contact): CarrierInterface;
	public function makeCall(): Call;
	public function sendMessage(string $message): Message;
}