<?php

namespace App;


class Call
{
    private string $contactNumber;

    /**
     * Call constructor.
     * @param string $contactNumber
     */
    public function __construct(string $contactNumber)
    {
        $this->contactNumber = $contactNumber;
    }

    /**
     * @return string
     */
    public function contactNumber(): string
    {
        return $this->contactNumber;
    }

}