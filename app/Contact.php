<?php

namespace App;


class Contact
{
    private string $name;
    private string $number;

    /**
     * Contact constructor.
     * @param string $name
     * @param string $number
     */
    public function __construct(string $name, string $number)
    {
        $this->name = $name;
        $this->number = $number;
    }

    /**
     * @return string
     */
    public function name(): string
    {
        return $this->name;
    }

    /**
     * @return string
     */
    public function number(): string
    {
        return $this->number;
    }
}