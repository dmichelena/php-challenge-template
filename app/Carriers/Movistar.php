<?php

namespace App\Carriers;

use App\Call;
use App\Contact;
use App\Interfaces\CarrierInterface;
use App\Message;

class Movistar implements CarrierInterface
{
    private Contact $contact;
    public function selectContact(Contact $contact): CarrierInterface
    {
        $this->contact = $contact;

        return $this;
    }

    public function makeCall(): Call
    {
        return new Call($this->contact->number());
    }

    public function sendMessage(string $message): Message
    {
        return new Message($this->contact->number(), $message);
    }
}