<?php

namespace App\Carriers;

use App\Call;
use App\Contact;
use App\Interfaces\CarrierInterface;
use App\Message;

class Claro implements CarrierInterface
{
    private Contact $contact;
    public function selectContact(Contact $contact): CarrierInterface
    {
        $this->contact = $contact;

        return $this;
    }

    public function makeCall(): Call
    {
        throw new \Exception('No signal');
    }

    public function sendMessage(string $message): Message
    {
        return new Message($this->contact->number(), $message);
    }
}