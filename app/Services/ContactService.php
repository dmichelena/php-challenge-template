<?php

namespace App\Services;

use App\Contact;


class ContactService
{
	private static $lookUp = [
	  'Diego' => '945874717',
	  'Alonso' => '995544221',
	  'Error' => '99554',
    ];
	public static function findByName(string $name): ?Contact
	{
        return isset(static::$lookUp[$name]) ? new Contact($name, static::$lookUp[$name]) : null;
	}

	public static function validateNumber(string $number): bool
	{
        return preg_match('/^9\d{8}$/', $number);
	}
}