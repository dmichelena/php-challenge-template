<?php

namespace App;


class Message
{
    private string $contactNumber;
    private string $message;

    /**
     * Message constructor.
     * @param string $contactNumber
     * @param string $message
     */
    public function __construct(string $contactNumber, string $message)
    {
        $this->contactNumber = $contactNumber;
        $this->message = $message;
    }

    /**
     * @return string
     */
    public function contactNumber(): string
    {
        return $this->contactNumber;
    }

    /**
     * @return string
     */
    public function message(): string
    {
        return $this->message;
    }

}