<?php

namespace App;

use App\Interfaces\CarrierInterface;
use App\Services\ContactService;


class Mobile
{

	protected $provider;
	
	function __construct(CarrierInterface $provider)
	{
		$this->provider = $provider;
	}


	public function makeCallByName(string $name)
	{
        $contact = $this->contact($name);
		$this->provider->selectContact($contact);

		return $this->provider->makeCall();
	}

    public function sendSMS(string $name, string $message)
    {
        if (empty($message)) {
            throw new \Exception('Message can not be empty');
        }
        $contact = $this->contact($name);
        if (!ContactService::validateNumber($contact->number())) {
            throw new \Exception('Contact number is invalid');
        }

        $this->provider->selectContact($contact);

        return $this->provider->sendMessage($message);
    }

    private function contact(string $name)
    {
        if (empty($name)) {
            throw new \Exception('Please, select one contact');
        }
        $contact = ContactService::findByName($name);
        if ($contact) {
            return $contact;
        }

        throw new \Exception('Contact not found');
    }

}
