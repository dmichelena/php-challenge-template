<?php

namespace Tests;

use App\Call;
use App\Carriers\Claro;
use App\Carriers\Movistar;
use App\Message;
use App\Mobile;
use PHPUnit\Framework\TestCase;

class MobileTest extends TestCase
{
    /** @test */
    public function it_throws_exception_when_contact_name_is_empty_calling()
    {
        $mobile = $this->movistarMobile();
        $this->expectExceptionMessage('Please, select one contact');
        $mobile->makeCallByName('');
    }

    /** @test */
    public function it_throws_exception_when_contact_not_exists_calling()
    {
        $mobile = $this->movistarMobile();
        $this->expectExceptionMessage('Contact not found');
        $mobile->makeCallByName('ASD');
    }

    /** @test */
    public function it_throws_exception_when_carrier_does_not_have_signal_calling()
    {
        $mobile = $this->claroMobile();
        $this->expectExceptionMessage('No signal');
        $mobile->makeCallByName('Diego');
    }

    /** @test */
    public function it_returns_call_model_when_everything_is_ok()
    {
        $mobile = $this->movistarMobile();
        $call = $mobile->makeCallByName('Diego');
        $this->assertInstanceOf(Call::class, $call);
        $this->assertEquals($call->contactNumber(), '945874717');
    }

    /** @test */
    public function it_throws_exception_when_contact_name_is_empty_sending_sms()
    {
        $mobile = $this->movistarMobile();
        $this->expectExceptionMessage('Please, select one contact');
        $mobile->sendSMS('', 'Message');
    }

    /** @test */
    public function it_throws_exception_when_message_is_empty_sending_sms()
    {
        $mobile = $this->movistarMobile();
        $this->expectExceptionMessage('Message can not be empty');
        $mobile->sendSMS('', '');
    }

    /** @test */
    public function it_throws_exception_when_contact_not_exists_sending_sms()
    {
        $mobile = $this->movistarMobile();
        $this->expectExceptionMessage('Contact not found');
        $mobile->sendSMS('ASD', 'Message');
    }

    /** @test */
    public function it_throws_exception_when_contact_number_is_invalid_sending_sms()
    {
        $mobile = $this->movistarMobile();
        $this->expectExceptionMessage('Contact number is invalid');
        $mobile->sendSMS('Error', 'Message');
    }

    /** @test */
    public function it_returns_message_model_when_everything_is_ok()
    {
        $mobile = $this->movistarMobile();
        $messageText = 'Message';
        $message = $mobile->sendSMS('Diego', $messageText);
        $this->assertInstanceOf(Message::class, $message);
        $this->assertEquals($message->contactNumber(), '945874717');
        $this->assertEquals($message->message(), $messageText);
    }


    private function claroMobile() {
        return new Mobile(new Claro());
    }

    private function movistarMobile() {
        return new Mobile(new Movistar());
    }

}
